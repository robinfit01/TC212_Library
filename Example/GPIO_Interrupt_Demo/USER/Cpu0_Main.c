/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC212
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-12-12
 ********************************************************************************************************************/


#include "headfile.h"


//工程导入到软件之后，应该选中工程然后点击refresh刷新一下之后再编译
//工程默认设置为关闭优化，可以自己右击工程选择properties->C/C++ Build->Setting
//然后在右侧的窗口中找到C/C++ Compiler->Optimization->Optimization level处设置优化等级
//一般默认新建立的工程都会默认开2级优化，因此大家也可以设置为2级优化

//对于TC系列默认是不支持中断嵌套的，希望支持中断嵌套需要在中断内使用enableInterrupts();来开启中断嵌套
//简单点说实际上进入中断后TC系列的硬件自动调用了disableInterrupts();来拒绝响应任何的中断，因此需要我们自己手动调用enableInterrupts();来开启中断的响应。
extern uint8 flag_3;
extern uint8 flag_7;

int core0_main(void)
{
	get_clk();//获取时钟频率  务必保留

	//用户在此处调用各种初始化函数等

	//特别注意通道0 与 通道2都被摄像头占用，由于中断共用的关系，因此通道4 与 通道6也不能使用
    //TC212的外部中断不像K60单片机那样所有IO都支持，TC212只有部分支持具体可以查看zf_eru.h中ERU_PIN_enum枚举定义

    eru_init(ERU_CH3_REQ6_P02_0,  RISING);   //ERU通道3 使用P02_0引脚  上升沿中断
    eru_init(ERU_CH7_REQ11_P20_9, RISING);   //ERU通道7 使用P20_9引脚  上升沿中断

    //中断函数在isr.c中
    //函数名称分别为eru_ch3_ch7_isr

    //中断相关的配置参数在isr_config.h内
    //可配置参数有 ERU_CH3_CH7_INT_SERVICE 和 ERU_CH3_CH7_INT_PRIO
    //ERU_CH3_CH7_INT_SERVICE 中断服务者
    //ERU_CH3_CH7_INT_PRIO       中断优先级 优先级范围1-255 越大优先级越高 与平时使用的单片机不一样

    //ERU其他中断也是同理

    //需要特备注意的是  不可以有优先级相同的中断函数 每个中断的优先级都必须是不一样的

	enableInterrupts();

	while (TRUE)
	{
		//用户在此处编写任务代码
        if(flag_3 == 1)
	    {
	        printf("eru ch3 be triggered\n");
	        flag_3 = 0;
	    }

        if(flag_7 == 1)
        {
            printf("eru ch7 be triggered\n");
            flag_7 = 1;
        }
        //程序运行之后，使用杜邦线将P20_9、P02_0连接到GND，然后在断开，在连接如此往复
        //将提示信息通过串口打印，可以先学习printf例程，了解如何使用printf
	}
}



