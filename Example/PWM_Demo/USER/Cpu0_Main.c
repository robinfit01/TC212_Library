/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC212
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-12-12
 ********************************************************************************************************************/


#include "headfile.h"


//工程导入到软件之后，应该选中工程然后点击refresh刷新一下之后再编译
//工程默认设置为关闭优化，可以自己右击工程选择properties->C/C++ Build->Setting
//然后在右侧的窗口中找到C/C++ Compiler->Optimization->Optimization level处设置优化等级
//一般默认新建立的工程都会默认开2级优化，因此大家也可以设置为2级优化

//对于TC系列默认是不支持中断嵌套的，希望支持中断嵌套需要在中断内使用enableInterrupts();来开启中断嵌套
//简单点说实际上进入中断后TC系列的硬件自动调用了disableInterrupts();来拒绝响应任何的中断，因此需要我们自己手动调用enableInterrupts();来开启中断的响应。

int core0_main(void)
{
	get_clk();//获取时钟频率  务必保留

	//用户在此处调用各种初始化函数等

	gtm_pwm_init(TOM0_CH8_P11_2,  50,    0);//TOM 0模块的通道8 使用P11_2引脚输出PWM  PWM频率50HZ  占空比百分之0/GTM_TOM0_PWM_DUTY_MAX*100  GTM_TOM0_PWM_DUTY_MAX宏定义在zf_gtm_pwm.h
    gtm_pwm_init(TOM0_CH10_P11_3, 1000,  0);
    gtm_pwm_init(TOM0_CH11_P11_6, 10000, 0);
    gtm_pwm_init(TOM0_CH12_P11_9, 30000, 0);

    gtm_pwm_init(TOM1_CH1_P33_9, 50, 5000);
    //每个通道都可以输出不同频率的PWM

    pwm_duty(TOM0_CH8_P11_2, 5000);//设置占空比为百分之5000/GTM_TOM0_PWM_DUTY_MAX*100
    pwm_duty(TOM0_CH10_P11_3, 5000);
    pwm_duty(TOM0_CH11_P11_6, 5000);
    pwm_duty(TOM0_CH12_P11_9, 5000);

	enableInterrupts();

	while (TRUE)
	{
		//用户在此处编写任务代码

	}
}



